from simulation import *
import pandas as pd
conso = pd.read_csv("./data/Ouessant_data_2016_Load.csv").to_numpy()

data_test = pd.read_csv("./data/exp2 copy.csv")
data_test = data_test.fillna(0)

X = data_test.loc[:, ["Architecture/PV (kW)", "Architecture/G10",
                      "Architecture/Gen100 (kW)", "Architecture/1kWh LI"]].to_numpy()

a = np.ones((len(X), 1))
X = np.hstack((X, a))
NPC = data_test.loc[:, ["Cost/NPC ($)"]].to_numpy()
LCOE = data_test.loc[:, ["Cost/COE ($)"]].to_numpy()
kf = 1/(1+discount_rqte)

for i in range(len(X)):
    print(" ")
    print(" ==== ")
    cost, cov = simulation_from_vec([X[i][2], X[i][1] / 100, X[i][0], X[i][3], X[i][4]], [
        data_wind, load_solar()], conso)
    discount_rate25Y = (1-kf**25)/(1-kf)
    LCOE_sim = cost/(discount_rate25Y*cov*sum(conso))
    print("i : ", i)
    print("LCOE : ", LCOE_sim, "réel", LCOE[i])
    print("NPC : ", cost, " réel : ", NPC[i])
    print("erreur relative LCOE", i, (LCOE_sim-LCOE[i])/LCOE[i])
    print("erreur relativeNPC", i, (cost-NPC[i])/NPC[i])
discount_rate25Y = (1-(1/(1+discount_rqte))**25)/(1-(1/(1+discount_rqte)))
