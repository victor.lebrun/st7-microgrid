from re import S
from capex import *
from opex import *
from dispatch import *


def simulation(configuration, f_dispatch, meteo, conso):
    etat = {"etat_generator": 0, "etat_charge": 0, "etat_deficit": 0}

    vec = simyear(configuration, meteo, conso, f_dispatch, datacost)
    total_cost = calcost(vec, configuration, echo=False)

    total_def = 0
    for e in vec[5]:
        if e < 0:
            total_def -= e
    total_demand = sum(conso)

    return total_cost, 1 - total_def/total_demand


def simulation_from_vec(vconf, meteo, conso):
    ratiocapa = 2
    configuration = {"generator": {"power": vconf[0]}, "wind": {"power": vconf[1]}, "solar": {
        "power": vconf[2]}, "battery": {"capacity": vconf[3], "power": vconf[3] / ratiocapa}}
    f_dispatch = dispatch_seuil
    if vconf[4] > 0:
        f_dispatch = dispatch_suivi

    def pack(composition, meteo, conso, etat, time): return f_dispatch(
        composition, meteo, conso, etat, time, 0, 0)

    return simulation(configuration, pack, meteo, conso)


def simulation_from_vec_mix(vconf, meteo, conso):
    ratiocapa = 2
    configuration = {"generator": {"power": vconf[0]}, "wind": {"power": vconf[1]}, "solar": {
        "power": vconf[2]}, "battery": {"capacity": vconf[3], "power": vconf[3] / ratiocapa}}

    def pack(composition, meteo, conso, etat, time): return dispatch_mix(
        composition, meteo, conso, etat, time, vconf[4], vconf[5])

    return simulation(configuration, pack, meteo, conso)


if __name__ == "__main__":

    # Ce code permet de lancer une simulation sur un vecteur de dimensionnement du système

    conso = pd.read_csv("./data/Ouessant_data_2016_Load.csv").to_numpy()

    cost, cov = simulation_from_vec_mix([1385.26954115,    0., 3139.37782532, 1142.68345498,
                                         21.63945934,   19.33576171], [
        data_wind, load_solar()], conso)

    discount_rate25Y = (1-(1/(1+discount_rqte))**25)/(1-(1/(1+discount_rqte)))
    print("cost = ", cost)
    print("coverage = ", cov)
    print("LCOE", cost/(discount_rate25Y*cov*sum(conso)))
