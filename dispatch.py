from opex import *
from wind import *
from solar import *

composition_chargemax = 3

generator = 0
wind = 1
solar = 2
battery = 3
dt = 1


def reserve(composition, meteo, conso, etat):
    return conso * 1.2


def dispatch_seuil(composition, meteo, conso, etat, time, f1=0, f2=0, seuil=0.8):
    eo = power_wind(composition["wind"], meteo[0])
    sl = power_solar(composition["solar"], meteo[1])
    gen = composition["generator"]["power"]
    puissance_enr = eo + sl

    if etat["etat_generator"] == gen:
        # Si le générateur est allumé
        if eo + sl + gen - conso > composition["battery"]["capacity"] - etat["etat_charge"]:
            # Limitation puissance générateur
            return [min(composition["battery"]["capacity"] - etat["etat_charge"], composition["generator"]["power"]), eo, sl, 0]

        return [gen, eo, sl, 0]

    else:
        # Si le générateur est éteint
        if conso - puissance_enr > composition["battery"]["power"] or conso - puissance_enr > etat["etat_charge"] * seuil:
            # Activation générateur
            return [gen, eo, sl, 0]

        return [0, eo, sl, 0]

    return [0, 0, 0, 0]


def dispatch_suivi(composition, meteo, conso, etat, time, f1=0, f2=0):
    eo = power_wind(composition["wind"], meteo[0])
    sl = power_solar(composition["solar"], meteo[1])
    gen = composition["generator"]["power"]
    puissance_enr = eo + sl

    bt = min(composition["battery"]["power"], etat["etat_charge"])

    solde = puissance_enr + bt - conso
    if solde < 0:
        gen = min(gen, - solde)
    else:
        gen = 0

    return [gen, eo, sl, 0]


def dispatch_mix(composition, meteo, conso, etat, time, f1, f2):
    time_in_day = time % 24
    if time_in_day == 0:
        time_in_day = 24
    if f1 <= time_in_day < f1+f2:
        return dispatch_seuil(composition, meteo, conso, etat, time, f1, f2)
    else:
        return dispatch_suivi(composition, meteo, conso, etat, time, f1, f2)
