

# Constant to test a generator
gen_om = 0
cost_fuel = 1
f1 = 0.253
f0 = 0.028
gen_rep = 500
gen_life = 15000
gen_max = 1e4


def generator_marginal(configuration, commande):
    return power_generator(configuration, commande)*cost_fuel*f1


def generator_fixed(configuration, commande):
    # Je crois que gen_rep/gen_life doit être enlevé, cela permettait de calculer les coûts de remplacement
    return gen_om + f0*cost_fuel*configuration["power"]


def power_generator(configuration, commande):
    if commande < 0:
        return 0
    elif commande > configuration["power"]:
        return configuration["power"]
    else:
        return commande


def generator_cost(configuration, commande):
    return generator_marginal(configuration, commande) + generator_fixed(configuration, commande)
