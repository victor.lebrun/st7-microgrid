from array import array
from distutils.command.config import config
from dispatch import *
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from solar import *
from wind import *
from battery import *
from generator import *
from output import *
from capex import *
from math import *


generator = 0
wind = 1
solar = 2
battery = 3

# constant operation and maintenance
# $/kwh/yr
pOM_battery = 10
life_battery = 25

# $/kw/yr
pOM_wind = 50
pOM_solar = 20
pOM_gen = 0

discount_rqte = 0.05


def opex(configuration, f_dispatch, meteo, conso):
    etat = 0
    return f_dispatch(configuration, meteo, conso, etat)


def cost_remplacement_batt_25Y(power_flux_1Y, configuration):
    "power_flux_1Y : somme sur l'année de la puissance en valeur absolue heure par heure qui passe par la batterie"

    if power_flux_1Y < 1:
        lifetime_batt = life_battery  # years
    else:
        lifetime_batt = min(life_battery,
                            3000*configuration/(power_flux_1Y))  # years
    #print("lifetime bat", lifetime_batt)
    cost = 0
    if lifetime_batt == life_battery:
        return 0
    for i in range(1, int(25/lifetime_batt) + 1):
        cost += configuration*pbat*(1/(1+discount_rqte)**(i*lifetime_batt))

    return cost


def cost_remplacement_gen_25Y(heure_utilisé_gen_1Y, configuration_gen):
    "heure_utilisé_gen_1Y : scalar "
    "configuration_gen : puissance installé gen, scalar"
    if heure_utilisé_gen_1Y < 1:
        lifetime_gen = 25
    else:
        lifetime_gen = min(25, 15000/heure_utilisé_gen_1Y)
    #print("h : ", heure_utilisé_gen_1Y)
    cost = 0
    if lifetime_gen == 25:
        return 0
    for i in range(1, int(25/lifetime_gen) + 1):
        cost += configuration_gen*pgen * \
            (1/(1+discount_rqte)**(i*lifetime_gen))
    return cost


def cost_operation_maintenance(configuration):
    cost = 0
    cost += pOM_battery*configuration["battery"]["capacity"]
    cost += pOM_gen*configuration["generator"]["power"]
    cost += pOM_solar*configuration["solar"]["power"]
    cost += pOM_wind*configuration["wind"]["power"]*1000
    return cost


def extrapolation_25Y(Opex_1Y):
    "Opex_1Y : scalar"
    cost_project = 0
    for i in range(1, 26):
        cost_project += Opex_1Y/(1+discount_rqte)**i
    return(cost_project)


def simhour(configuration, commande, meteo, conso, etat):
    eo = min(power_wind(configuration["wind"], meteo[0]), commande[wind])
    sl = min(power_solar(configuration["solar"], meteo[1]), commande[solar])
    gn = power_generator(configuration["generator"], commande[generator])
    production = eo + sl + gn
    cmd = power_battery(configuration["battery"], production - conso, etat)
    etat["etat_generator"] = gn
    etat["etat_deficit"] = production - conso - cmd

    return etat


def simyear(configuration, meteo, conso, dispatchf, dataf):
    vec = []
    etat = {"etat_generator": 0, "etat_charge": 0, "etat_deficit": 0}

    for i in range(0, 8759):
        commande = dispatchf(
            configuration, [meteo[0][i], meteo[1][i]], conso[i], etat, i)
        simhour(configuration, commande, [
            meteo[0][i], meteo[1][i]], conso[i], etat)
        dataf(configuration, commande, etat, vec)

    return vec


def datanocost(configuration, commande, etat, vec):
    if len(vec) == 0:
        vec += [[], [], [], [], [], []]

    vec[0] += [float(etat["etat_generator"])]
    vec[1] += [float(etat["etat_charge"])]
    vec[5] += [float(etat["etat_deficit"])]
    vec[2] += [commande[wind]]
    vec[3] += [float(commande[solar])]
    if len(vec[4]) == 0:
        vec[4] += [etat["etat_charge"]]
    else:
        vec[4] += [etat["etat_charge"] - vec[0][-2]]


def datacost(configuration, commande, etat, vec):
    if len(vec) == 0:
        vec += [[], [], [], [], [], [], 0, 0, 0, 0, 0, 0]

    vec[0] += [float(etat["etat_generator"])]
    vec[1] += [float(etat["etat_charge"])]
    vec[5] += [float(etat["etat_deficit"])]
    vec[2] += [commande[wind]]
    vec[3] += [float(commande[solar])]
    if len(vec[4]) == 0:
        vec[4] += [etat["etat_charge"]]
    else:
        vec[4] += [etat["etat_charge"] - vec[1][-2]]

    if commande[0] > 0:
        vec[6] += 1
        vec[9] += generator_fixed(configuration["generator"], commande[0]) + \
            generator_marginal(
            configuration["generator"], commande[0])
    vec[7] += float(commande[0])
    vec[8] += abs(vec[4][-1])
    vec[10] += commande[1]
    vec[11] += commande[2]


def calcost(vec, configuration, echo=False):
    cost1 = cost_remplacement_batt_25Y(
        vec[8], configuration["battery"]["capacity"])
    cost2 = cost_remplacement_gen_25Y(vec[6],
                                      configuration["generator"]["power"])
    cost3 = cost_operation_maintenance(configuration)
    t = extrapolation_25Y(
        cost3+vec[9])+capex(configuration)+cost1+cost2
    if echo:
        print("rep bat", cost1)
        print("rep gen", cost2)
        print("om", cost3)
        print("fuel", vec[9])
        print("solar cap", vec[11])
        print("wind", vec[10])
        print("capex", capex(configuration))
        print(t)

    return t


if __name__ == "__main__":

    # Ce code permet de lancer une simulation physique sur une année

    etat = {"etat_generator": 0, "etat_charge": 0, "etat_deficit": 0}

    configuration = {"generator": {"power": 739}, "wind": {"power": 0.97}, "solar": {
        "power": 755}, "battery": {"capacity": 647, "power": 647}}
    solar_1k = load_solar()

    conso = pd.read_csv("./data/Ouessant_data_2016_Load.csv").to_numpy()

    vec = simyear(configuration, [data_wind, solar_1k],
                  conso, dispatch_seuil, datacost)

    calcost(vec, configuration, echo=False)

    plot_data(vec[0], vec[1], vec[2], vec[3], vec[4], vec[5])
    plot_data(mean_h24(vec[0]), mean_h24(vec[1]), mean_h24(vec[2]),
              mean_h24(vec[3]), mean_h24(vec[4]), mean_h24(vec[5]), h=False)

    plt.show()
