from simulation import *
from scipy import optimize
from timeit import *

w = 0.99


def cost_f(x, meteo, conso):
    cost, cov = simulation_from_vec(x, meteo, conso)
    return (cost + (1-cov)*sum(conso)*30)


def cost_g(x, meteo, conso):
    cost, cov = simulation_from_vec_mix(x, meteo, conso)
    return (cost + (1-cov)*sum(conso)*30)


def opt_neldermead(conso, meteo, fct):
    return optimize.minimize(f, [0, 0, 10000, 0, 1], bounds=optimize.Bounds([0, 0, 0, 0, -1], [0, 0, 1e5, 1e5, 1]), tol=100, method='Nelder-Mead')


def opt_shgo(conso, meteo, fct):
    return optimize.shgo(f, [(0, 1e5), (0, 10), (0, 1e5), (0, 1e5), (-1e-5, 1e-5)])


def opt_annealing(conso, meteo, fct):
    return optimize.dual_annealing(
        f, bounds=list(zip([0, 0, 0, 0, -1e-5], [1e4, 1, 1e3, 1e3, 1e-5])), maxiter=1, initial_temp=1, callback=p)


def opt_annealingmix(conso, meteo, fct):
    return optimize.dual_annealing(
        g, bounds=list(zip([0, 0, 0, 0, 0, 0], [1e4, 10, 1e4, 1e4, 24, 24])), maxiter=1, initial_temp=1, callback=p)


if __name__ == "__main__":

    # Ce code permet de trouver un dimensionnement optimisé
    # mix permet de choisir si on travaille sur du dispatch cycle/LF ou sur un mix

    conso = pd.read_csv("./data/Ouessant_data_2016_Load.csv").to_numpy()

    meteo = [data_wind, load_solar()]

    def f(x): return cost_f(x, meteo, conso)
    def g(x): return cost_g(x, meteo, conso)
    def p(x, f, c): print("x =", x, "; f =", f, "; c =", c)

    mix = False
    if mix:
        res = opt_annealingmix(conso, meteo, g)
    else:
        res = opt_annealing(conso, meteo, f)

    print(res)
    print("> Vecteur de dimensionnement : ")
    print(res.x)  # Ce vecteur peut être récupéré pour être envoyer dans les codes de simulation ou d'opex pour avoir plus de détails
    print(">> Puissance du générateur : ", res.x[0])
    print(">> Puissance des éoliennes : ", res.x[1] * 1000)
    print(">> Puissance des panneaux solaires : ", res.x[2])
    print(">> Puissance des batteries : ", res.x[3])
    if mix:
        print(">> Heure 1 : ", res.x[4])
        print(">> Heure 2 : ", res.x[5])
    else:
        if res.x[4] > 0:
            s = "suivi de charge"
        else:
            s = "seuil/cycle"
        print(">> Algorithme : ", s)
