import pandas as pd
import numpy as np


def solar_pro(Size_of_PV, solar_1k):
    """
    calculate solar output and its marginal cost
    input:  Size of pv, positive number, kw
    output: Production_PV, list of 8759, the maximum output of a specific hour, kw
    """
    Production_PV = Size_of_PV*solar_1k

    return Production_PV


def solar_cost(Size_of_PV):
    """
    calculate solar output and its marginal cost
    input:  Size of pv, positive number, kw
    output: Marginal cost, possitive number, it's fixed once we fix the size of PV, $/h
    """
    Pom_pv = 20
    marginal_cost = Size_of_PV*Pom_pv/(8760)
    return marginal_cost


def load_solar():
    data = pd.read_csv("./data/Ouessant_data_2016_Ppv1k.csv")
    solar_1k = data.to_numpy()
    return solar_1k


def power_solar(configuration, meteo):
    return meteo * configuration["power"] / 1000 * 2.476


if __name__ == '__main__':
    import pandas as pd
    import numpy as np
    import os as os

    '''
        data = pd.read_csv("./data/Ouessant_data_2016_Ppv1k.csv")
        solar_1k = data.to_numpy()
        print(solar_pro(10,solar_1k=solar_1k).shape)
        print(solar_cost(10).shape)
        '''
