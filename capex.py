generator = 0
wind = 1
solar = 2
battery = 3

# constant to calculate opex

pgen = 500
pWind = 1500*1000
pSolar = 1200
pbat = 350


def capex_generator(composition):
    return composition*pgen


def capex_wind(composition):
    return composition*pWind


def capex_solar(composition):
    return composition*pSolar


def capex_battery(composition):
    return composition*pbat


def capex(configuration):
    c = capex_generator(configuration["generator"]["power"])
    c += capex_wind(configuration["wind"]["power"])
    c += capex_solar(configuration["solar"]["power"])
    c += capex_battery(configuration["battery"]["capacity"])
    return c
