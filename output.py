from matplotlib import pyplot as plt


def plot_data(s1, s2, s3, s4, s5, s6, h=True):

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
    ax1.plot(s2, 'g', label="Energie")
    ax1.set_ylabel("Energie (kWh)")
    ax1.legend()
    ax1.set_title("Stockage")

    ax2.plot(s1, 'r', label="Générateur")
    ax2.plot(s3, 'b', label="Vent")
    ax2.plot(s4, 'y', label="Soleil")
    ax2.set_ylabel("Puissance (kW)")
    ax2.legend()
    ax2.set_title("Production")

    st = []
    for i in range(len(s1)):
        st += [s1[i] + s3[i] + s4[i]]
    ax3.plot(st, 'g', label="Production totale")
    ax3.plot(s5, 'b', label="Flux batterie")
    ax3.plot(s6, 'r', label="Excédent/Pénurie")
    if h:
        ax3.set_xlabel("Temps (h)")
    else:
        ax3.set_xlabel("Temps (j)")
    ax3.set_ylabel("Puissance (kW)")
    ax3.set_title("Flux généraux")

    ax3.legend()


def mean_h24(s):
    out = []
    for i in range(len(s) // 24):
        out += [0]
        for j in range(24):
            out[i] += s[i * 24 + j]
    return out
