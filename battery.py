

def power_battery(configuration, commande, etat):
    cap = etat["etat_charge"]

    # Saturation puissance
    commande = min(commande, configuration["power"])
    commande = max(commande, -configuration["power"])

    # Saturation charge
    if cap + commande > configuration["capacity"]:
        commande = configuration["capacity"] - cap

    # Saturation décharge
    if cap + commande < 0:
        commande = -cap

    # Exécution
    etat["etat_charge"] += commande
    return commande
