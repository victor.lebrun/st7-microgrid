import pandas as pd
import numpy as np
from scipy import interpolate

# Constant to test wind
data = pd.read_csv("./data/Ouessant_data_2016_Wind.csv")
speed = data.to_numpy()
pOM_wind = 10  # $/kwh/y


data_wind = pd.read_csv("./data/Ouessant_data_2016_Wind.csv")
data_wind = data_wind.to_numpy()
speed = [0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 20, 25]

power_1k = [0, 0, 80, 150, 250, 400, 600, 850,
            1150, 1350, 1450, 1490, 1500, 1500, 1500, 1500]

# convert speed at 2meter height to speed at 80m
data_wind = (80/2)**0.1*data_wind

f = interpolate.interp1d(speed, power_1k)


def power_wind(configuration, meteo):
    if meteo[0] < 0:
        return 0
    if 0<=meteo[0]<=25:
        return f(meteo)*configuration["power"]
    if meteo[0] > 25 :
        return 0


def eolienne_cost(configuration):
    return configuration*pOM_wind
